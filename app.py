# importando bibliotecas
from tkinter import *
import tkinter as tk
from tkinter import ttk
from view import *
from tkinter import messagebox
from pdf import gerar_pdf
# cloudes
branco = "#ffffff" # branco
branco = "#000000" # Preta
cloud = "#2c3e50" # Midnigth blue
inserir = '#2ecc71' # Emerald
alterar = "#f1c40f" # Sun flower
alerta = '#e74c3c' # Alizarin
cloud = '#ecf0f1' # Cloud

# Janelas\cgi-sys\defaultwebpage.cgi

janela = Tk()
janela.title("Impressão Cartela - Versão 0.0.01")
janela.geometry()
janela.configure(background=cloud)
janela.resizable(width=True,height=True)

# Frames da Janela
frame_cima = Frame(janela, width=310, height=50, bg=cloud, relief='flat')
frame_cima.grid(row=0, column=0, padx=0, pady=1)

frame_esquerda = Frame(janela, width=310, height=500, bg=cloud, relief='flat')
frame_esquerda.grid(row=1, column=0,sticky=NSEW, padx=0, pady=1)

frame_direita = Frame(janela, width=588, height=500, bg=cloud, relief='flat')
frame_direita.grid(row=0, column=1, rowspan=2, padx=1, pady=0, sticky=NSEW)

# Label Cima
app_nome = Label(frame_cima, text='Formulário de Cadastro', anchor=NW, font=('roboto 13 bold'), bg=cloud, fg=branco,  relief='flat')
app_nome.place(x=20,y=15)

##############
# VARIAVEL GLOBAL
global tree

###############
# FUNÇÃO INSERIR
def gravar_banco():
    nome = e_nome.get()
    bairro = e_bairro.get()
    cidade = e_cidade.get()
    uf = e_uf.get()
    endereco = e_endereco.get()
    cpf = e_cpf.get()
    email = e_email.get()
    telefone = e_telefone.get()
    vendedor = e_vendedor.get()
    
    dados = [nome,bairro,cidade,uf,endereco,cpf,email,telefone,vendedor]
    
    if nome == '':
        messagebox.showerror("Erro", "Nome não pode ser vazio")
    else:
        insert_info(dados)
        messagebox.showinfo("Sucesso","Dados inseridos com sucesso!")
        e_nome.delete(0, 'end')
        e_bairro.delete(0, 'end')
        e_cidade.delete(0, 'end')
        e_uf.delete(0, 'end')
        e_endereco.delete(0, 'end')
        e_cpf.delete(0, 'end')
        e_email.delete(0, 'end')
        e_telefone.delete(0, 'end')
        e_vendedor.delete(0, 'end')
    
    for widget in frame_direita.winfo_children():
        widget.destroy()
    show()

# FUNÇÃO ATUALIZAR
def atualizar_banco():
    try:
        treev_dados = tree.focus()
        treev_dicionario = tree.item(treev_dados)
        tree_lista = treev_dicionario['values']
        
        valor_id = tree_lista[0]
        
        e_nome.delete(0, 'end')
        e_bairro.delete(0, 'end')
        e_cidade.delete(0, 'end')
        e_uf.delete(0, 'end')
        e_endereco.delete(0, 'end')
        e_cpf.delete(0, 'end')
        e_email.delete(0, 'end')
        e_telefone.delete(0, 'end')
        e_vendedor.delete(0, 'end')
        
        e_nome.insert(0, tree_lista[1])
        e_bairro.insert(0, tree_lista[2])
        e_cidade.insert(0, tree_lista[3])
        e_uf.insert(0, tree_lista[4])
        e_endereco.insert(0, tree_lista[5])
        e_cpf.insert(0, tree_lista[6])
        e_email.insert(0, tree_lista[7])
        e_telefone.insert(0, tree_lista[8])
        e_vendedor.insert(0, tree_lista[9])
        
        def atualiza_bd():
            nome = e_nome.get()
            bairro = e_bairro.get()
            cidade = e_cidade.get()
            uf = e_uf.get()
            endereco = e_endereco.get()
            cpf = e_cpf.get()
            email = e_email.get()
            telefone = e_telefone.get()
            vendedor = e_vendedor.get()
    
            dados = [nome,bairro,cidade,uf,endereco,cpf,email,telefone,vendedor, valor_id]
    
            if nome == '':
                messagebox.showerror("Erro", "Nome não pode ser vazio")
            else:
                atualizar_info(dados)
                
                messagebox.showinfo("Sucesso", "Dados Atualizados com sucesso!")
                e_nome.delete(0, 'end')
                e_bairro.delete(0, 'end')
                e_cidade.delete(0, 'end')
                e_uf.delete(0, 'end')
                e_endereco.delete(0, 'end')
                e_cpf.delete(0, 'end')
                e_email.delete(0, 'end')
                e_telefone.delete(0, 'end')
                e_vendedor.delete(0, 'end')
    
            for widget in frame_direita.winfo_children():
                widget.destroy()
            
            show()
            
            # Atualizar

        b_confirmar = Button(frame_esquerda, command=atualiza_bd, text="Confirmar", width=10, font=('roboto 10 bold'),  bg=alterar, fg=branco,  relief='raised', overrelief='ridge')
        b_confirmar.place(x=100, y=405, width=100)

    except IndexError:
        messagebox.showerror('Erro','Selecione os dados na tabela')
        print("erro")

# FUNÇÃO DELETAR
def deletar_banco():
    try:
        treev_dados = tree.focus()
        treev_dicionario = tree.item(treev_dados)
        tree_lista = treev_dicionario['values']
        
        valor_id = [tree_lista[0]]
        
        delete_info(valor_id)
        messagebox.showerror('Sucesso','Dados Deletados com sucesso')
        
        for widget in frame_direita.winfo_children():
            widget.destroy()
        
        show()
    
    except IndexError:
        messagebox.showerror('Erro','Selecione os dados na tabela')
        print("erro")

# FUNÇÃO FILTRO
def procurar_banco():
    pass

def pdf_banco():
    try:
        treev_dados = tree.focus()
        treev_dicionario = tree.item(treev_dados)
        tree_lista = treev_dicionario['values']
        
        valor_id = tree_lista[0]
        
        e_nome.delete(0, 'end')
        e_bairro.delete(0, 'end')
        e_cidade.delete(0, 'end')
        e_uf.delete(0, 'end')
        e_endereco.delete(0, 'end')
        e_cpf.delete(0, 'end')
        e_email.delete(0, 'end')
        e_telefone.delete(0, 'end')
        e_vendedor.delete(0, 'end')
        
        e_nome.insert(0, tree_lista[1])
        e_bairro.insert(0, tree_lista[2])
        e_cidade.insert(0, tree_lista[3])
        e_uf.insert(0, tree_lista[4])
        e_endereco.insert(0, tree_lista[5])
        e_cpf.insert(0, tree_lista[6])
        e_email.insert(0, tree_lista[7])
        e_telefone.insert(0, tree_lista[8])
        e_vendedor.insert(0, tree_lista[9])
    
        def pdf_():
            nome = e_nome.get()
            bairro = e_bairro.get()
            cidade = e_cidade.get()
            uf = e_uf.get()
            endereco = e_endereco.get()
            cpf = e_cpf.get()
            email = e_email.get()
            telefone = e_telefone.get()
            vendedor = e_vendedor.get()
            
            dados = [nome,bairro,cidade,uf,endereco,cpf,email,telefone,vendedor, valor_id]
            
            if nome == '':
                messagebox.showerror("Erro", "Nome não pode ser vazio")
            else:
                atualizar_info(dados)
                gerar_pdf(nome, bairro, cidade, uf, endereco, cpf, email, telefone, vendedor)
                messagebox.showinfo("Sucesso","PDF GERADO com sucesso!")
                
        # Impressão PDF
        b_imprimir = Button(frame_esquerda,command=pdf_, text='Imprimir', width=10, font=('roboto 10 bold'),  bg=cloud, fg=branco,  relief='raised', overrelief='ridge')
        b_imprimir.place(x=200, y=410, width=70)
    
    except IndexError:
        messagebox.showerror('Erro','Selecione os dados na tabela')
        print("erro")
        


# Label Esquerda

# Labal e Entry Nome
l_nome = Label(frame_esquerda, text='Nome Completo:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_nome.place(x=10,y=10)
e_nome = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_nome.place(x=10,y=30)

# Labal e
l_bairro = Label(frame_esquerda, text='Bairro:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_bairro.place(x=10,y=50)
e_bairro = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_bairro.place(x=10,y=70)

# Labal e Entry Cidade 
l_cidade = Label(frame_esquerda, text='Cidade:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_cidade.place(x=10,y=90)
e_cidade = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_cidade.place(x=10,y=110)

# Labal e Entry UF 
l_uf = Label(frame_esquerda, text='UF:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_uf.place(x=10,y=130)
e_uf = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_uf.place(x=10,y=150)

# Labal e Entry Endereço 
l_endereco = Label(frame_esquerda, text='Endereço:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_endereco.place(x=10,y=170)
e_endereco = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_endereco.place(x=10,y=190)

# Labal e Entry CPF 
l_cpf = Label(frame_esquerda, text='CPF:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_cpf.place(x=10,y=210)
e_cpf = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_cpf.place(x=10,y=230)

# Labal e Entry Email
l_email = Label(frame_esquerda, text='Email:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_email.place(x=10,y=250)
e_email = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_email.place(x=10,y=270)

# Labal e Entry Telefone
l_telefone = Label(frame_esquerda, text='Telefone:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_telefone.place(x=10,y=290)
e_telefone = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_telefone.place(x=10,y=310)

# Labal e Entry Vendedor
l_vendedor = Label(frame_esquerda, text='Vendedor:', anchor=NW, font=('roboto 10 bold'), bg=cloud, fg=branco,  relief='flat')
l_vendedor.place(x=10,y=330)
e_vendedor = Entry(frame_esquerda, width=48, justify='left' ,relief='solid')
e_vendedor.place(x=10,y=350)


##########################################
## Botões
##########################################

# Inserir
b_inserir = Button(frame_esquerda,command=gravar_banco, text="Gravar", width=10, font=('roboto 10 bold'),  bg=inserir, fg=branco,  relief='raised', overrelief='ridge')
b_inserir.place(x=5, y=375, width=50)

# Atualizar
b_update = Button(frame_esquerda,command=atualizar_banco, text="Editar", width=10, font=('roboto 10 bold'),  bg=alterar, fg=branco,  relief='raised', overrelief='ridge')
b_update.place(x=60, y=375, width=50)

# Deletar
b_pesquisar = Button(frame_esquerda,command=deletar_banco, text="Deletar", width=10, font=('roboto 10 bold'),  bg=alerta, fg=branco,  relief='raised', overrelief='ridge')
b_pesquisar.place(x=115, y=375, width=70)

# Imprimir
b_gpdf = Button(frame_esquerda,command=pdf_banco, text="PDF", width=10, font=('roboto 10 bold'),  bg=cloud, fg=branco,  relief='raised', overrelief='ridge')
b_gpdf.place(x=195, y=375, width=70)


##########################################
# Tree view
##########################################

def show ():
    global tree
    cadastros = show_info()

    

    # Lista para Cabeçalho
    tabela_head = ['Id','Nome','Bairro','Cidade','UF','Endereço','CPF','Email','Telefone','Vendedor']


    tree = ttk.Treeview(frame_direita,selectmode="extended",
                        columns=tabela_head,
                        show="headings")
    # Vertical Scrol bar
    vsb = ttk.Scrollbar(frame_direita, orient="vertical", command=tree.yview)
    # Horizontal Scrol bar
    hsb = ttk.Scrollbar(frame_direita, orient="horizontal", command=tree.xview)
    tree.configure(yscrollcommand=vsb.set, xscrollcommand=hsb.set)

    tree.grid(column=0, row=0 , sticky='NSEW')
    vsb.grid(column=1, row=0 , sticky='NS')
    hsb.grid(column=0, row=1 , sticky='EW')
    frame_direita.grid_rowconfigure(0, weight=12)

    hd = ['w', 'w', 'w','w', 'w', 'w','w','w','w','w']
    h=30,150,100,100,75,100,100,100,100,100
    n=0

    for col in tabela_head:
        tree.heading(col, text= col.title(), anchor=tk.W)
        # Ajustando a coluna no tamanho da strings
        tree.column(col, width=h[n], anchor=hd[n],  stretch=1)
        
        n+=1

    for item in cadastros:
        tree.insert('', 'end', values=item)


show()


janela.mainloop()