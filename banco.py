import sqlite3 as lite

# Conexao
con = lite.connect('banco_dev.db')

create_db_clientes = """
CREATE TABLE IF NOT EXISTS clientes (
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    nome TEXT NOT NULL ,
    bairro TEXT  ,
    cidade TEXT  ,
    uf TEXT NOT NULL DEFAULT MT ,
    endereco TEXT ,
    cpf TEXT ,
    email TEXT ,
    telefone TEXT NOT NULL,
    vendedor TEXT DEFAULT ZOINHO
);
"""


# Crindo tabelas

with con:
    cur = con.cursor()
    cur.execute(create_db_clientes)