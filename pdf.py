#!/usr/bin/python
# -*- coding: UTF-8 -*-
from fpdf import FPDF
from pathlib import Path
fontfolder = Path('fonts/Roboto/')

def gerar_pdf(nome,bairro,cidade,uf,endereco,cpf,email,telefone,vendedor):
    pdf = FPDF(orientation="P", unit="mm", format=(148,210))
    pdf.add_font('Roboto-Regular', '', fontfolder/'Roboto-Regular.ttf')
    #pdf.add_font(family='Helvetica',style="Bold",fname=None)
    pdf.set_font("Roboto-Regular", '',7)
    pdf.add_page()
    #pdf.image('assets\img\Template.jpg', x=0, y=0, w=0, h=0)
    pdf.text(18,165,nome.capitalize())
    pdf.text(12,171,bairro.capitalize())
    pdf.text(12,177,cidade.capitalize())
    pdf.text(85,177,uf.upper())
    pdf.text(12,183,endereco.capitalize())
    pdf.text(95,183,cpf)
    pdf.set_font("Roboto-Regular", '', 6)
    pdf.text(10,189,email.lower())
    pdf.set_font("Roboto-Regular", '', 7)
    pdf.text(64,189,telefone)
    pdf.text(16,196,vendedor.capitalize())
    
    pdf.output(f"{nome}.pdf")