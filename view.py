import sqlite3 as lite

# DEFININDO CONEXAO COM BANCO
con = lite.connect('banco_dev.db')

# FUNÇÃO INSERT
def insert_info(i):
    with con:
        cur = con.cursor()
        query = "INSERT INTO clientes ( nome, bairro, cidade, uf, endereco, cpf, email, telefone, vendedor) VALUES (?,?,?,?,?,?,?,?,?)"
        cur.execute(query, i)

# FUNÇÃO READ
def show_info():
    with con:
        dados = []
        cur = con.cursor()
        query = "SELECT * FROM clientes"
        resp = cur.execute(query).fetchall()
        
        for i in resp:
            dados.append(i)
    return dados

# FUNÇÃO UPDATE
def atualizar_info(i):
    with con:
        cur = con.cursor()
        query = "UPDATE clientes SET nome=?, bairro=?, cidade=?, uf=?, endereco=?, cpf=?, email=?, telefone=?, vendedor=? WHERE id=?"
        cur.execute(query, i)

# FUNÇÃO DELET
def delete_info(i):
    with con:
        cur = con.cursor()
        query = "DELETE FROM clientes WHERE id=?"
        cur.execute(query, i)